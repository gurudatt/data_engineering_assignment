# Data Engineering Assignment

### This assignment tests your skills in ingesting data from an external database.

Permitted tools are Apache Spark and Apache Kafka.


### Connection Details

You can connect to the Postgres instance running in our servers, using the following configuration.


**Host** : dcastdb5362.cloudapp.net 

**Port** : 5432 

**Username** : test 

**Password** : zIe7FZyhCZBrnR1OEdaDssujvHz 

**Database name** : assignment


Data are generated and pushed to the database at a random time.
Once you start the script, it will continue for 6 hours before stopping. 

You can start the script by visiting [this URL](http://dcastdb5362.cloudapp.net:5000/database/start)

The database entries can be reset by going [here](http://dcastdb5362.cloudapp.net:5000/database/reset)

Report any bugs as an issue on this repo.


### Assignment Instructions
There are two tables in the database.
You need to:
1. Explore and understand the structure of the data
2. Join the tables in Spark using the column common to both tables
3. Write code for Spark (in Scala or Java) to join tables and output the data as JSON
4. Share the code as a new private repo in gitlab.
5. The repo should also contain 
    - the gzipped tarball of the joined JSON, and 
    - detailed documentation of your process and code
    
Send all questions on email to gurudatt [at] dataculture [dot] in.